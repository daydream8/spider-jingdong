# 批量爬取京东商品（Jsoup）

#### 介绍
一个简单批量爬取京东商品的名称、价格、商品的展示的图片url(s)，可用于毕业设计等项目，作测试数据。



#### 项目结构

```
├─icu
|  ├─daydream
|  |    ├─Demo.java						执行项目
|  |    ├─util
|  |    |  ├─AcquireInitData.java		获取网站数据
|  |    |  ├─ExcelUtil.java				写入文件
|  |    |  └StringUtils.java			字符串工具
|  |    ├─pojo		
|  |    |  ├─GoodsInfo.java				商品信息实体类
|  |    |  ├─GoodsMedia.java			图片实体类
|  |    |  └Item.java
```



#### 项目初始化

1.  将项目clone到本地，仓库地址：https://gitee.com/endless_daydreaming/spider-jingdong.git
3.  以maven方式打开项目



#### 说明：

本项目爬取范围为 https://search.jd.com/ 域名下的数据.

如图👇

![image-20210324085409390.png](https://gitee.com/endless_daydreaming/spider-jingdong/raw/master/img/image-20210324085409390.png)

![image-20210324085607892.png](https://gitee.com/endless_daydreaming/spider-jingdong/raw/master/img/image-20210324085607892.png)



#### 使用介绍：

1. ​	将需要爬取的网址放入excel文件中的第一列，如图👇

	![image-20210324090036044.png](https://gitee.com/endless_daydreaming/spider-jingdong/raw/master/img/image-20210324090036044.png)

2. 打开项目中 **demo.java**文件，修改路径，**urlsFile** 为需要爬取的url文件的本地路径，**goodsInfoFile** 为数据保存的文件路径，如图👇

	![image-20210324090622124.png](https://gitee.com/endless_daydreaming/spider-jingdong/raw/master/img/image-20210324090613449.png)

3. 执行代码.

4. 爬取数据结果，如图👇

	| ![image-20210324091424730.png](https://gitee.com/endless_daydreaming/spider-jingdong/raw/master/img/image-20210324091424730.png) |
	| ------------------------------------------------------------ |
	| ![image-20210324091532282.png](https://gitee.com/endless_daydreaming/spider-jingdong/raw/master/img/image-20210324091532282.png) |

	






##### 个人网站：http://www.daydream.icu/articles

##### 个人博客：https://www.cnblogs.com/endless-daydream