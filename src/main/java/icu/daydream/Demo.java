package icu.daydream;

import icu.daydream.pojo.Item;
import icu.daydream.util.AcquireInitData;
import icu.daydream.util.ExcelUtil;
import icu.daydream.util.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Demo
 * @Description TODO
 * @Author 白日梦
 * @Date 2021-03-23 22:34
 * @Version 1.0.0
 */
public class Demo {

    public static void main(String[] args) throws IOException, InterruptedException, InvalidFormatException {
        String urlsFile = "D:\\test\\urls.xlsx";
        String goodsInfoFile = "D:\\test\\goods_info.xlsx";
        //获取id集合

        getAndWrite(urlsFile,goodsInfoFile);
    }




    /**
     * @apiNote 从urlsFile文件（excel）读取数据，并将数据写入 goodsInfoFile（excel）文件中
     * @param urlsFile
     * @param goodsInfoFile
     * @return
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static void getAndWrite(String urlsFile,String goodsInfoFile) throws IOException, InvalidFormatException {
        long start = System.currentTimeMillis();
        AcquireInitData acquireInitData = AcquireInitData.getInstance();
        List<String> urls = ExcelUtil.getJINGDONGUrlsFromExcel(urlsFile);
        System.out.println("["+ StringUtils.getCurTime()+"]"+">>>>>商品列表url数量:"+urls.size());
        System.out.println("["+StringUtils.getCurTime()+"]"+">>>>>商品列表url"+urls);
        List<String> list = new ArrayList();
        for (String url : urls) {
            List<String> l = acquireInitData.acquireGoodsSkuIdList(url,0);
            list.addAll(l);
        }
        System.out.println("["+StringUtils.getCurTime()+"]"+"*=*=一共"+list.size()+"条数据*=*=");
        List<Item> items = new ArrayList<>();
        int i =1;
        for (String s : list) {
            System.out.println("["+StringUtils.getCurTime()+"]"+"*=*=正在获取第 "+i+" 条数据*=*=");
            Item item = acquireInitData.parseGoodsDetailHTML(s);
            items.add(item);
            try {Thread.sleep(100);} catch (InterruptedException e) {}
            i++;
        }
        ExcelUtil.writeItems(items,goodsInfoFile);
        long end = System.currentTimeMillis();
        String times = StringUtils.secondToTime((end - start) / 1000);
        System.out.println(">>>>>完成耗时："+times);
    }


}
