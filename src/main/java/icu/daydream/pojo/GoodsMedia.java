package icu.daydream.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @ClassName GoodsMediaInfo
 * @Description TODO
 * @Author 白日梦
 * @Date 2021-03-04 13:26
 * @Version 1.0.0
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GoodsMedia {
    Integer id;
    String goodsId;
    Integer type;
    String fileName;
    String url;
    String uploadTime;

    public GoodsMedia(Integer id, String goodsId, Integer type) {
        this.id = id;
        this.goodsId = goodsId;
        this.type = type;
    }
}
