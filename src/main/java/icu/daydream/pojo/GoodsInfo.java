package icu.daydream.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName GoodsInfo
 * @Description TODO
 * @Author 白日梦
 * @Date 2021-03-04 13:18
 * @Version 1.0.0
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GoodsInfo {
    String id;  //商品编号
    String name;
    String norms;
    String unit;
    String brand;
    Integer saleStatus;
    Double curPrice;
    Integer stockNumber;
    Integer goodsType;
    String releaseTime;
    String updateTime;
    String imgUrl;
    List<GoodsMedia> medias = new ArrayList<>();

    public GoodsInfo(String id, String name, String brand, Integer saleStatus, Double curPrice, Integer goodsType, List<GoodsMedia> medias) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.saleStatus = saleStatus;
        this.curPrice = curPrice;
        this.goodsType = goodsType;
        this.medias = medias;
    }
}
