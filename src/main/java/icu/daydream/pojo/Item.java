package icu.daydream.pojo;

/**
 * @ClassName Item
 * @Description TODO
 * @Author 白日梦
 * @Date 2021-03-05 23:41
 * @Version 1.0.0
 */

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * ~@Data:
 * lombok，自动生成getter/setter
 *
 * ~@Entity:
 * 表示当前Pojo和数据库哪张表对应。系统启动后JPA读取到@Entity标注的Pojo时，会根据Pojo字段在数据库建一张表。
 * 你可以用@Entity(name = "jd_item")指定表名称，否则默认以Pojo名称作为表名。
 *
 * ~@Id:
 * JPA无法推测哪个是主键，必须指定
 *
 * ~@Column:
 * 和@Entity差不多，就是名称映射。你不写的话，生成的表字段就是Pojo的字段名，不是spu_id，而是spuId
 *
 * ~@CreationTimestamp:
 * 插入的时候时间自动生成，所以Pojo不用在程序里设置时间了
 *
 * ~@UpdateTimestamp:
 * 更新Pojo时自动更新时间
 */
@Data
public class Item {
    /**
     * 主键id
     */
    private Long id;


    /**
     * spuId
     */
    private Long spuId;

    /**
     * skuId
     */
    private String skuId;

    /**
     * 商品标题
     */
    private String title;

    /**
     * 商品价格
     */
    private Double price;

    /**
     * 商品图片
     */
    private List<String> images ;

    /**
     * 商品详情地址
     */
    private String url;


    /**
     * 商品属性
     */
    private Map<String,String> properties;


}
