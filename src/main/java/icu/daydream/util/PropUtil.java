package icu.daydream.util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * @ClassName PropUtil
 * @Description TODO
 * @Author 白日梦
 * @Date 2021-04-20 16:41
 * @Version 1.0.0
 */
public class PropUtil {
    private static final String GOODS_PROP_NAME = "goods_prop_name";

    public static final String[] props ;

    static {
        String[] arr= readProperties();
        if (arr == null){
            arr = new String[]{};
        }
        props = arr;
    }


    private static String[] readProperties(){
        Properties prop = new Properties();
        //读取配置
        InputStream is = PropUtil.class.getResourceAsStream("/goods.properties");
        try {
            prop.load(new InputStreamReader(is, "UTF-8"));
            Object value = prop.get(GOODS_PROP_NAME);
            if (value == null || "".equals(value)){
                return null;
            }
            String[] split = value.toString().split(",");
            return  split;

        } catch (IOException e) {
            return null;
        }

    }

    public static void main(String[] args) {
        readProperties();
    }

}
