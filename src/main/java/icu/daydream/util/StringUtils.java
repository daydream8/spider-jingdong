package icu.daydream.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * @ClassName StringUtil
 * @Description
 * @Author 白日梦
 * @Date 2021-03-07 15:08
 * @Version 1.0.0
 */
public class StringUtils {

    /**
     * @apiNote 获取当前时间
     * @return
     */
    public static String getCurTime(){
        SimpleDateFormat sdf = new SimpleDateFormat();// 格式化时间
        sdf.applyPattern("yyyy-MM-dd HH:mm:ss");// a为am/pm的标记
        Date date = new Date();// 获取当前时间
        return sdf.format(date);
    }
    /**
     * 秒转换为指定格式的日期
     * @param second
     * @param patten
     * @return
     */
    public String secondToDate(long second,String patten) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(second * 1000);//转换为毫秒
        Date date = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat(patten);
        String dateString = format.format(date);
        return dateString;
    }
    /**
     * 返回日时分秒
     * @param second
     * @return
     */
    public static String secondToTime(long second) {
        long days = second / 86400;//转换天数
        second = second % 86400;//剩余秒数
        long hours = second / 3600;//转换小时数
        second = second % 3600;//剩余秒数
        long minutes = second / 60;//转换分钟
        second = second % 60;//剩余秒数
        if (0 < days) {
            return days + "day " + hours + "h " + minutes + "min " + second +"s ";
        } else {
            return hours + "h " + minutes + "min " + second +"s ";
        }
    }

    /**
     * 获取指定范围的随机整数
     * @param from
     * @param to
     * @return
     */
    public static int getRandomNumber(int from ,int to){
        if(from > to){
            int tmp = from ; from = to; to= tmp;
        }
        Random random = new Random();
        int s = random.nextInt(to)%(to-from+1) + from;
        return s;
    }

    /**
     * @apiNote 判断字符串是否有效，为空返回false，不为空返回true
     * @param string
     * @return
     */
    public static boolean isValid(String string){
        if(string ==null || string.trim().equals("")){
            return false;
        }
        return true;
    }

    /**
     * @apiNote 判断字符串是否有效，为空返回false，不为空返回true
     * @param strArray
     * @return
     */
    public static boolean isAllValid(String ...strArray){
        for (String s : strArray) {
            if(!isValid(s)){
                return false;
            }
        }
        return true;
    }



    public static String[] concat(String[] a, String[] b) {
        String[] c= new String[a.length+b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    public static void main(String[] args) throws InterruptedException {
        int randomNumber = getRandomNumber(0,20);
        System.out.println(randomNumber);
    }
}
