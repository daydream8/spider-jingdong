package icu.daydream.util;

import com.google.gson.Gson;
import icu.daydream.pojo.Item;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName AcquireInitData
 * @Description 爬取商品数据，并对数据进行解析
 * @Author 白日梦
 * @Date 2021-03-04 1:56
 * @Version 1.0.0
 */

public class AcquireInitData {
    //单例懒汉模式
    private static AcquireInitData acquireInitData ;
    //private static final String JING_DONG_BASE_URL =  "https://search.jd.com/search?keyword=%E8%B6%85%E5%B8%82&qrst=1&wq=%E8%B6%85%E5%B8%82&stock=1&stock=1&ev=exbrand_%E8%89%AF%E5%93%81%E9%93%BA%E5%AD%90%5E&cid3=1595";
    Gson gson = new Gson();
    /**
     * @apiNote 获取AcquireInitData对象
     * @return
     */
    private AcquireInitData(){}
    public static AcquireInitData getInstance(){
        if (acquireInitData == null ){
            synchronized (AcquireInitData.class){
                acquireInitData = new AcquireInitData();
            }
        }
        return acquireInitData;
    }




    /**
     * @apiNote  获取商品超链接集合
     * @param page 页
     * @return List<String>链接集合
     * @author 白日梦
     */
    private List<String> acquireGoodsURLList(String url ,int page){
        Document document = null;
        //爬取page页的商品
        url =url+"&page="+page;
        System.out.println("["+StringUtils.getCurTime()+"]"+">>>>>url为："+url);
        try {
            document = Jsoup.connect(url).get();
            Elements glItems = document.getElementsByClass("gl-item");
            List<String> list = new ArrayList<>();
            for (Element glItem : glItems) {
                Elements aList = glItem.getElementsByTag("a");
                Element a = aList.get(2);
                String href = a.attr("href");
                href = "http:"+href.replaceAll("html.*","html");
                list.add(href);
            }
            return list;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @apiNote 获取商品id集合
     * @param url
     * @param page
     * @return
     */
    public List<String> acquireGoodsSkuIdList(String url ,int page){
        //获取商品超链接集合
        List<String> oList = acquireGoodsURLList(url ,page);
        if (oList ==null){
            return null;
        }
        Pattern pattern = Pattern.compile("com/(.*).html");
//        System.out.println(oList);
        List list = new ArrayList();
        for (String href : oList) {
            Matcher matcher = pattern.matcher(href);
            if (matcher.find()){
//                System.out.println(matcher.group(1));
//                System.out.println(matcher.groupCount());
                list.add(matcher.group(1));
            }
        }
        return list;
    }


    /**
     * @apiNote 获取默认尺寸大小的图片信息 n=1
     * @param document
     * @return
     */
     public List<String> getGoodsImages(Document document){
        return getGoodsImages(document,1);
    }

    /**
     * @apiNote 获取不同尺寸大小的图片信息
     * @param document  文档对象
     * @param n n的取值范围为1-9
     * @return
     */
    private List<String> getGoodsImages(Document document,int n){
        if (document == null){
            return null;
        }
        List<String> list = new ArrayList<>();
        Elements imgElements = document.select("#spec-list   img");
        for (Element imgElement : imgElements) {
            String src = imgElement.attr("src");
            src ="https:"+ src.replaceAll("/n[1-9]/", "/n"+n+"/");
            list.add(src);
        }
        return list;
    }

    /**
     * @apiNote 获取商品价格
     * @param skuId 商品id
     * @return
     */
    private Double getPrice(String skuId) {
        //商品价格url
        String priceUrl = "https://p.3.cn/prices/mgets?skuIds=J_" +skuId;
        String text = null;
        try {
            text = Jsoup.connect(priceUrl).ignoreContentType(true).userAgent("Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.2.15)").timeout(5000).get().text();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        System.out.println(text);
//        List<HashMap<String,String>> json = new ArrayList<HashMap<String,String>>();
        List list = gson.fromJson(text,List.class);
        Map<String,String> map = (Map)list.get(0);
        String p = map.get("p");
        return Double.parseDouble(p);

    }


    /**
     * @apiNote 获取商品属性
     * @param document
     * @return
     */
    public Map<String,String> getGoodsProperties(Document document){
        try {
            Elements p_parameters = document.getElementsByClass("p-parameter");
            Element  p_parameter = p_parameters.get(0);
            Elements propsNodes = p_parameter.getElementsByTag("li");

            Map<String,String> propMap = new LinkedHashMap<>();
            for (Element propsNode : propsNodes) {
                String[] split = propsNode.text().split("：");
                propMap.put(split[0],split[1]);
            }
            Map<String,String > res = new LinkedHashMap<>();
            for (String prop : PropUtil.props) {
                res.put(prop,propMap.get(prop));
            }
            return  res;
        }catch (Exception e){
            return  new LinkedHashMap<String,String>();
        }
    }



    /**
     * 获取商品id的详细信息
     * @param skuId 商品id
     * @return
     */
    public Item parseGoodsDetailHTML(String skuId){
        //商品详情url
        String url = "https://item.jd.com/" + skuId + ".html";
        Item item = new Item();
        try {
            Document document = Jsoup.connect(url).get();
            //获取标题
            String title = document.select("div.sku-name").text();
            item.setTitle(title);
            //获取图片
            item.setImages(this.getGoodsImages(document));
            //获取价格
            item.setPrice(this.getPrice(skuId));
            item.setSkuId(skuId);
            item.setUrl(url);
            //获取属性
            item.setProperties(this.getGoodsProperties(document));
            return item;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



}
